<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"
<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="MobileOptimized" content="width" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="cleartype" content="on" />
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- Jquery -->
    <script src="sites/all/themes/simplify/script/jquery-1.8.0.min.js"></script> 
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
	<div id="skip-link">
		<a href="#main-content" class="element-invisible element-focusable"> <?php print t('Skip to main content'); ?> </a>
	</div>
	<?php print $page_top; ?>
	<?php print $page; ?>
	<?php print $scripts; ?>
	<?php print $page_bottom; ?>
</body>
</html>