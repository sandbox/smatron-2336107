	<!-- HEADER -->
	<header class="nav-bar main-header">
		<div class="container">
		    	<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
		    	<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="logo left"/>
		    </a>
		    		    
            <!-- MENU -->
            <?php if (!empty($main_menu) || !empty($secondary_menu)): ?>
            <nav class="nav-bar">
                <a class="toggle-menu" href="#">Menu</a>
                <div class="nav">
                    <!-- User Menu -->
                    <?php if ($secondary_menu): ?>
                    <?php print theme('links__system_secondary_menu', array(
                        'links' => $secondary_menu,
                        'attributes' => array(
                        'class' => array('user-menu'),
                        ),
                    )); ?>
                    <?php endif; ?>
                    
                    <!-- Main Menu -->
                    <?php if ($main_menu): ?>
                    <?php print theme('links__system_main_menu', array(
                        'links' => $main_menu,
                        'attributes' => array(
                        'class' => array('main-menu'),
                        ),
                    )); ?>
                    <?php endif; ?>
                </div>
            </nav>
            <?php endif; ?>
	    
		  </div>
	</header>
	
	<!-- SLIDESHOW -->
	<section>
	   <?php if ($page['slideshow']): ?>
	       <?php print render($page['slideshow']); ?>
	   <?php endif; ?>
	</section>

	<!-- CONTENT AREA -->
    <section>
        <div class="middle-container section-content">
            <div class="container">
            <div class="row section">
            <a id="main-content"></a>
            
            <?php print $breadcrumb; ?>
            <?php print render($title_prefix); ?>
            <?php if ($title): ?>
            <h1 class="title" id="page-title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print render($title_suffix); ?>
            
            <?php if ($tabs): ?>
                <div class="tabs">
                <?php print render($tabs); ?>
                </div>
            <?php endif; ?>
            
            <?php if ($action_links): ?>
            <ul class="action-links">
              <?php print render($action_links); ?>
            </ul>
            <?php endif; ?>
            
            <?php print render($page['content']); ?>
            <div class="span8">
                <?php print render($page['content_left']); ?>
            </div>
            
            <!-- Sidebar -->
            <?php if ($page['sidebar_second']): ?>
            <div id="sidebar-second" class="span4 column sidebar">
                <div class="section">
                    <?php print render($page['sidebar_second']); ?>
                </div>
            </div>
            <?php endif; ?>
            </div>
            
            <?php print $feed_icons; ?>
 
            </div>
        </div>
    </section>

    <section class="bottom-container">
        <?php if ($page['content_bottom']): ?>
            <div class="container section-content">
                <?php print render($page['content_bottom']); ?>
            </div>
        <?php endif; ?>
    </section> 
    
    <!-- SUB FOOTER -->
    <section>
        <?php if ($page['sub_footer']): ?>
            <div class="sub-footer container section-content">
                <?php print render($page['sub_footer']); ?>
            </div>
        <?php endif; ?>
    </section>
    
    <!-- FOOTER -->
    <footer class="container main-footer" id="contact-info">
        <?php if ($page['footer']): ?>
        <div class="row">
            <?php print render($page['footer']); ?>
        </div>
        <?php endif; ?>
	</footer>
